# import the segno library for generating QR code
import segno

# Generate micro QR code
qrcode = segno.make('Yellow Submarine', error='q')
qrcode.save('yellow-submarine.png', border=5, scale=10, \
    dark='darkred', light='lightblue')

# save as an svg file
qrcode.save("yellow-submarine.svg")

# check the version and the error correction level
print(qrcode.designator)


