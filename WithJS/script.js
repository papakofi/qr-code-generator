function generateQr() {
  let chartAPI =
    'https://chart.googleapis.com/chart?cht=qr&chs=';
  
  let qrText = document.querySelector('.qrtext').value;

  let qrImage = document.querySelector('.img');

  let qrSize = document.querySelector('.frame-size').value;

  if (qrText !== '') {

    switch (qrSize) {
      case "100":
        qrImage.src = chartAPI + '100x100&chl=' + qrText;
        break;
      case "150":
        qrImage.src = chartAPI + '150x150&chl=' + qrText;
        break;
      case "200":
        qrImage.src = chartAPI + '200x200&chl=' + qrText;
        break;
      case "250":
        qrImage.src = chartAPI + '250x250&chl=' + qrText;
        break;
      case "300":
        qrImage.src = chartAPI + '300x300&chl=' + qrText;
        break;
      default:
        alert("Invalid text!")
        break;
    }
  }

  else {
    alert("Please enter text in the text field!");
  }
}
