import segno

# Generate a QR code for URL sharing
website = segno.make("https://www.datasustl.com/")
website.save("web-address.png", dark="blue", scale=7)