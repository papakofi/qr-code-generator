from segno import helpers

# Generate QR code for contact details
# Run the code below to generate the QR code for my contact information

details = {
    "name" : "Boahen, Papa Kofi",
    "email" : "papakofiboahen@gmail.com",
    "phone" : "+233503230804",
    "city" : "Accra",
    "country" : "Ghana",
    "memo" : "Computer Engineering Student"
}

contact = helpers.make_mecard(**details)

contact.save("contact.png", scale=7)